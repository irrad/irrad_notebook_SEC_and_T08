#!/usr/bin/env python
# coding: utf-8

# # IRRAD NOTEBOOK APPLICATION
# 
# The subsequent script aims to provide a file which contains the spills of the IRRAD facility. This file will be obtained after some data analysis between the SEC and T08 database throughout five steps.
# 
# To run each cell one-by-one, press Shift + Enter.
# 
# Please follow accurately the instructions given by each cell to get the file that you would like to publish in your website. Bear in mind that you need to wait for the message STEP n completed. Please move towards STEP n+1 to make sure that you ran the code properly. 
# 
# Please remove all the intermediate files every time you want to run the code.
# 
# If you have any comments/suggestions, please contact Pierre Pelissou - pierre.pelissou@cern.ch
# 

# In[1]:


import cx_Oracle
import datetime,time,sched
import getpass
from datetime import datetime


# In[2]:


user =  input('Please, specify database user:')
print('\nPlease, specify database password:')
password = getpass.getpass()
connection_string = user+"/"+password+"@pdbr-s.cern.ch:10121/pdbr.cern.ch"


# In[3]:


start_date = input('Please, specify starting datetime in local time and format YYYY-MM-DD HH24:MI:SS\n')
end_date = input('Please, specify end datetime in local time and format YYYY-MM-DD HH24:MI:SS\n')
month = input('Please, specify the month of the data:\n')


# STEP 1: YOU WILL USE THE COMMAND PYTIMBER TO EXTRACT YOUR DATA FROM T08.XSEC070

# In[4]:


#BEGINNING OF T08.XSEC070 IN A GIVEN CSV FILE 

get_ipython().system('curl -s https://gitlab.cern.ch/pelson/swan-run-in-venv/-/raw/master/run_in_venv.py -o .run_in_venv')

get_ipython().run_line_magic('run', '.run_in_venv pytimber-env -m pip install pytimber==3.2.28')
#%run .run_in_venv pytimber-env -m pip install pytimber==3.*

get_ipython().run_line_magic('run', '.run_in_venv pytimber-env -m pip install --upgrade pytimber')
get_ipython().run_line_magic('run', '.run_in_venv pytimber-env -m cmmnbuild_dep_manager resolve')

#%run .run_in_venv pytimber-env -m pip install pytimber==3.*

get_ipython().run_line_magic('run', '.run_in_venv pytimber-env -m pip install pytimber==3.2.28')



import pytimber
print()
print("Imported version of pytimber module:", pytimber.__version__)



# import pytimber as timb
import time
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as md
import re
import csv
from datetime import datetime, timedelta


# Query is going to return the requested data in the specified start and end time local time. Timestamp will be in unix time and UTC time (meaning -1h or -2h in case of summer time). 

db = pytimber.LoggingDB()
data = db.getAligned('T08.XSEC070-I:IntensityMeasurement:counts',start_date,end_date,'CPS:%:EAST3')    # query for data overspecific time period from timber


# In[5]:


#CREATING THE CSV FILE FOR T08.XSEC070 WITH RAW COUNTS AND TIMESTAMP CONVERTED INTO A STANDARD HH:MM:SS

#PLEASE NAME THE FILE T08.XSEC070_study


from datetime import datetime
from math import *


a=[]
counts_final=[]
res = data['timestamps']
counts = data['T08.XSEC070-I:IntensityMeasurement:counts']

for i in range(len(res)):
    x=datetime.fromtimestamp(round(res[i]))
    b=datetime.strptime(str(x), '%Y-%m-%d %H:%M:%S') 
    a.append(str(b))
    counts_final.append(counts[i])




labels= ("Timestamp,T08.XSEC070-I:IntensityMeasurement:counts\n")
print('Please name as the following: T08_XSEC070_study\n')
filename = input('Please, specify the file name where your data will be stored ')
fname = filename+".csv"


with open(fname, "a") as myfile:
    myfile.write(labels)
    for i in range(len(a)):
        strg = str(a[i]) + "," +  str(counts_final[i])
        myfile.write(strg)
        myfile.write("\n")
        


#CREATING A FILE WITH THE SAME T08.XSEC070 CONVERTED COUNTS

#PLEASE NAME THE FILE T08_XSEC070_converted_study


T08_XSEC070_converted=[]

K1=1.8306E-7
K2=1.983E7

for i in range(len(a)):
    x=round(counts_final[i]/(K1*K2))
    T08_XSEC070_converted.append(x)
    



        
labels= ("Timestamp,T08.XSEC070_converted_counts\n")
print('\n\nPlease name as the following: T08_XSEC070_converted_study\n')
filename = input('Please, specify the file name where your data will be stored ')
fname = filename+".csv"


with open(fname, "a") as myfile:
    myfile.write(labels)
    for i in range(len(a)):
        strg = str(a[i]) + "," +  str(T08_XSEC070_converted[i])
        myfile.write(strg)
        myfile.write("\n") 

print("\nFile created in SWAN project\n")
print("STEP 1 completed. Please move towards STEP 2\n\n")


# STEP 2: YOU WILL USE THE SQL COMMAND TO EXTRACT YOUR DATA FROM SEC

# In[6]:


#BEGINNING OF SEC ANALYSIS IN A GIVEN CSV FILE 

#PLEASE NAME THE FILE SEC_study

con = cx_Oracle.connect(user+"/"+password+"@pdbr-s.cern.ch:10121/pdbr.cern.ch") #pdbr
cur = con.cursor()

sql = "SELECT * FROM PS_IRRAD_ADMIN.SEC_DATA WHERE  TIMESTAMP>TO_DATE('"+start_date+"', 'YYYY-MM-DD HH24:MI:SS')  AND TIMESTAMP<TO_DATE('"+end_date+"', 'YYYY-MM-DD HH24:MI:SS') AND SEC_ID='SEC_01'"
cur.execute(sql)
rows=cur.fetchall()

#TWO VARIABLES IN THE ROWS VARIABLE: TIMESTAMP AND SEC_VALUE

labels= ("TIMESTAMP,SEC_VALUE\n")
print('Please name as the following: SEC_study\n')
filename = input('Please, specify the file name where your data will be stored ')
fname = filename+".csv"

lst=[]
lst_final=[]

for e in range(len(rows)):
    lst = lst + [[*rows[e]]]

lst.sort()

for e in range(len(lst)-1):
    lst_final.append([lst[e][1],lst[e+1][2]])



index=0
with open(fname, "a") as myfile:
    myfile.write(labels)
        
    for i in range(len(lst)-1):
        index=index+1
        strg = str(lst[i][1]) + "," +  str(lst[i+1][2])
        myfile.write(strg)
        myfile.write("\n")    
        
        
        
        
cur.close()
con.close()
print("\nFile created in SWAN project\n")
print("STEP 2 completed. Please move towards STEP 3\n\n")


# STEP 3: REMOVE ALL THE SILLY POINTS FROM SEC WHICH HAVE 1 SECOND OF DIFFERENCE

# In[7]:


#THE FILE CONTAINS ALL THE SEC POINTS THAT HAVE 1 SECOND OF DIFFERENCE

#PLEASE NAME THE FILE silly_points


list_SEC=[]

for i in range(len(lst_final)):
    list_SEC.append([lst_final[i][0],lst_final[i][1]])




list_SEC_silly=[]
new_list=[]
list_SEC_1=[]
list_SEC_2=[]

#FOR SEC
for i in range(len(list_SEC)):
    SEC_date_sec=round(list_SEC[i][0].timestamp())
    new_list.append([lst_final[i][0],SEC_date_sec,lst_final[i][1]])
    
    
    
for i in range(len(new_list)-1):
    if (abs(new_list[i+1][1]-new_list[i][1]) == 1):
        list_SEC_silly.append([new_list[i][0],new_list[i][2],new_list[i+1][0],new_list[i+1][2]])
        list_SEC_1.append([new_list[i][0],new_list[i][2]])
        list_SEC_2.append([new_list[i+1][0],new_list[i+1][2]])


labels= ("TIMESTAMP_number1,SEC_VALUE_number1,TIMESTAMP_number2,SEC_VALUE_number2\n")
print('Please name as the following: silly_points\n')
filename = input('Please, specify the file name where your data will be stored ')
fname = filename+".csv"

with open(fname, "a") as myfile:
    myfile.write(labels)
        
    for i in range(len(list_SEC_silly)):
        strg = str(list_SEC_silly[i][0]) + "," +  str(list_SEC_silly[i][1]) + "," +  str(list_SEC_silly[i][2]) + "," +  str(list_SEC_silly[i][3])
        myfile.write(strg)
        myfile.write("\n")  



time_1=[]
time_2=[]
value_1=[]
value_2=[]

for i in range(len(list_SEC_1)):
    time_1.append(list_SEC_1[i][0])
    time_2.append(list_SEC_2[i][0])
    value_1.append(list_SEC_1[i][1])
    value_2.append(list_SEC_2[i][1])

print("\nFile created in SWAN project\n")
print("STEP 3 completed. Please move towards STEP 4\n\n")


# STEP 4: CREATE AN INTERMEDIATE SEC FILE WITHOUT THE SILLY POINTS

# In[8]:


#CREATE A NEW FILE WITH THE SILLY POINTS OF OF SEC ONLY

import pandas as pd


labels= ("TIMESTAMP,SEC_VALUE\n")
print('Please name as the following: silly_SEC_study\n')
filename = input('Please, specify the file name where your data will be stored ')
fname = filename+".csv"

with open(fname, "a") as myfile:
    myfile.write(labels)
        
    for i in range(len(list_SEC_2)):
        index=index+1
        strg = str(list_SEC_2[i][0]) + "," +  str(list_SEC_2[i][1])
        myfile.write(strg)
        myfile.write("\n")  




#HERE WE CREATE A DATAFRAME OF THE ORIGINAL SEC FILE

SEC=pd.read_csv('SEC_study.csv')
df1=SEC.rename(columns={"TIMESTAMP": "TIMESTAMP", "SEC_VALUE": "SEC_VALUE"})

#HERE WE CREATE A DATAFRAME OF THE SEC SILLY POINTS TO REMOVE FROM THE ORIGINAL FILE

SEC_wrong=pd.read_csv('silly_SEC_study.csv')
df2=SEC_wrong.rename(columns={"TIMESTAMP": "TIMESTAMP", "SEC_VALUE": "SEC_VALUE"})




#HERE WE IDENTIFY THE LINES WHICH ARE THE SAME BETWEEN THE SEC FILE OF THE LINES WE WANT TO REMOVE AND THE ORIGINAL SEC FILE


df3 = df2.assign(InDf1=df2.TIMESTAMP.isin(df1.TIMESTAMP).astype(int))
pd.set_option('display.max_rows', df3.shape[0]+1)


df4 = df1.assign(InDf2=df1.TIMESTAMP.isin(df2.TIMESTAMP).astype(int))
pd.set_option('display.max_rows', df4.shape[0]+1)


df2_not_included = df3[df3['InDf1'] == 0]
df2_included = df3[df3['InDf1'] > 0]

df1_not_included = df4[df4['InDf2'] == 0]
df1_included = df4[df4['InDf2'] > 0]


#HERE IS THE FILE (SEC_inter_file.cv) WHICH HAS BEEN CREATED FROM THE SUBSTRACTION OF SEC ORIGINAL FILE MINUS THE SILLY POINTS WE WANT TO REMOVE FROM SEC_study FILE

df1_not_included.to_csv('SEC_inter_file.csv',columns=['TIMESTAMP','SEC_VALUE'],index=False)
file_SEC=pd.read_csv('SEC_inter_file.csv')
df5=file_SEC.rename(columns={"TIMESTAMP": "TIMESTAMP", "T08.XSEC070_converted_Value": "SEC_VALUE"})

print("\nFile created in SWAN project\n")
print("STEP 4 completed. Please move towards STEP 5\n\n")


# STEP 5: CREATE BOTH FINAL FILES OF T08 AND SEC

# In[9]:


#CONVERT ALL THE STRING OF THE FIRST COLUMN OF SEC_FINAL_FILE INTO A DATE TIMETAMP FOR FURTHER CALCULATIONS

import datetime


liste_SEC_final=[]
file=pd.read_csv('SEC_inter_file.csv')
liste_SEC_final=file.to_numpy()


SEC=[]

for i in range(len(liste_SEC_final)):
    SEC.append([datetime.datetime.strptime(liste_SEC_final[i][0], '%Y-%m-%d %H:%M:%S'),liste_SEC_final[i][1]])


#REMOVE ALL THE COUNTS LOWER THAN 1000 FOR T08 AND AL THE COUNTS LOWER THAN 100 FOR SEC

liste_T08=[]
liste_T08_final=[]
SEC_final=[]

file=pd.read_csv('T08_XSEC070_converted_study.csv')


liste_T08=file.to_numpy()

for i in range(len(liste_T08)):
    if liste_T08[i][1] > 1000:
        liste_T08_final.append([liste_T08[i][0],liste_T08[i][1]])


for j in range(len(SEC)):
    if SEC[j][1] > 100:
        SEC_final.append([SEC[j][0],SEC[j][1]])

        
        
#NAME EACH FILE AS TE FOLLOWING AND IN THIS ORDER SEC_final and T08_final

labels= ("TIMESTAMP,SEC_VALUE\n")
print('Please name as the following: SEC_final\n')
filename = input('Please, specify the file name where your data will be stored ')
fname = filename+".csv"

with open(fname, "a") as myfile:
    myfile.write(labels)
        
    for i in range(len(SEC_final)):
        strg = str(SEC_final[i][0]) + "," +  str(SEC_final[i][1])
        myfile.write(strg)
        myfile.write("\n")  
        
labels= ("TIMESTAMP,T08.XSEC070_converted_Value\n")
print('\n\nPlease name as the following: T08_final\n')
filename = input('Please, specify the file name where your data will be stored ')
fname = filename+".csv"

with open(fname, "a") as myfile:
    myfile.write(labels)
        
    for i in range(len(liste_T08_final)):
        strg = str(liste_T08_final[i][0]) + "," +  str(liste_T08_final[i][1])
        myfile.write(strg)
        myfile.write("\n") 
        
print("\nFile created in SWAN project")


# In[10]:


#HERE WE CREATE A DATAFRAME WITH THE TIMESTAMP OF SEC AND THE COUNTS OF SEC

import pandas as pd

file_SEC=pd.read_csv('SEC_final.csv')
df_SEC=file_SEC.rename(columns={"TIMESTAMP": "TIMESTAMP", "SEC_VALUE": "SEC_Value"})


#HERE WE CREATE A DATAFRAME WITH THE TIMESTAMP OF T08 AND THE COUNTS OF T08

file_T08 = pd.read_csv('T08_final.csv')
df_TO8=file_T08.rename(columns={"TIMESTAMP": "TIMESTAMP", "T08.XSEC070_converted_Value": "T08.XSEC070_converted_Value"})

#HERE WE COUNT THE NUMBER OF LINES WHICH HAVE THE SAME TIMESTAMPS BETWEEN SEC AND TO8

merged_data = pd.merge(df_SEC,df_TO8, how = 'inner', on = ['TIMESTAMP'])
merged_data['TIMESTAMP'] = pd.to_datetime(merged_data['TIMESTAMP'], dayfirst=True)
pd.set_option('display.max_rows', merged_data.shape[0]+1)
#merged_data.info()
merged_data.to_csv('merged_data.csv')


#HERE WE COUNT THE NUMBER OF LINES WHICH ARE UNIQUE IN T08 AND SEC RESPECTIVELY

df3 = df_TO8.assign(InDf1=df_TO8.TIMESTAMP.isin(df_SEC.TIMESTAMP).astype(int))
pd.set_option('display.max_rows', df3.shape[0]+1)



df4 = df_SEC.assign(InDf2=df_SEC.TIMESTAMP.isin(df_TO8.TIMESTAMP).astype(int))
pd.set_option('display.max_rows', df4.shape[0]+1)


df2_not_included = df3[df3['InDf1'] == 0]
df2_included = df3[df3['InDf1'] > 0]

df1_not_included = df4[df4['InDf2'] == 0]
df1_included = df4[df4['InDf2'] > 0]



#FILE TO BE PUBLISHED

T08_list=[]
T08_list=df2_included.to_numpy()

labels= ("TIMESTAMP,T08.XSEC070_value\n")
filename = "TIMBER_DATA_"+month+"_2022"
fname = filename+".csv"

with open(fname, "a") as myfile:
    myfile.write(labels)
        
    for i in range(len(df2_included)):
        strg = str(T08_list[i][0]) + "," +  str(T08_list[i][1])
        myfile.write(strg)
        myfile.write("\n") 
        
print("\nFile created in SWAN project\n")
print("STEP 5 completed. Please move towards STEP 6 whether you would like to get a picture of the ratio and remove the intermediate files before running the script again\n\n")


# STEP 6: COMPARISON OF THE RATIO BETWEEN SEC AND T08 OVER TIME

# In[11]:


from datetime import datetime
import matplotlib.pyplot as plt
import matplotlib.dates


SEC=[]
T08_list=[]
SEC_count=[]
T08_list_count=[]
ratio=[]
time=[]


SEC=df1_included.to_numpy()
T08_list=df2_included.to_numpy()

for i in range(len(T08_list)):
    T08_list_count.append(T08_list[i][1])
    
for j in range(len(SEC)):
    SEC_count.append(SEC[j][1])
    
    
if len(SEC_count)>len(T08_list_count):
    length=len(T08_list_count)
else:
    length=len(SEC_count)

    

for k in range(length):
    ratio.append(SEC_count[k]/T08_list_count[k])
    time.append(T08_list[k][0])

    
datetime_object=[]

for i in range(len(time)):
    datetime_object.append(datetime.strptime(time[i], '%Y-%m-%d %H:%M:%S'))
    
fig=plt.figure(figsize=(30,10))
plt.gcf().subplots_adjust(left = 0.125, bottom = 0.1, right = 0.9, top = 0.9, wspace = 0.2, hspace = 1.0)
plt.rcParams['font.size'] = 14
ax=fig.add_subplot(2,1,1)
ay=fig.add_subplot(2,1,2)



dates = matplotlib.dates.date2num(datetime_object)
ax.plot_date(dates,ratio,'p', color="blue")
ax.set_title('Plot of the ratio SEC/T08 of ' +month+' '+start_date[0:4],color="black",fontsize=30)
ax.set_xlabel("Time",color="black",fontsize=30,loc="right")
ax.set_ylabel("Ratio",color="black",fontsize=30,loc="top")


dates = matplotlib.dates.date2num(datetime_object)
ay.plot_date(dates,ratio,'p', color="blue")
ay.set_title('Plot of the ratio SEC/T08 of ' +month+' '+start_date[0:4],color="black",fontsize=30)
ay.set_xlabel("Time",color="black",fontsize=30,loc="right")
ay.set_ylabel("Ratio",color="black",fontsize=30,loc="top")
ay.set_ylim((0,2))


# In[12]:


#IDENTIFY POINTS WHERE THE RATIO IS LESS THAN 0.5 AND GREATER THAN 1.3

list_final=[]

for i in range(len(ratio)):
    if ratio[i] > 1.25 or ratio[i] < 0.25:
        list_final.append([ratio[i],SEC_count[i],T08_list_count[i],datetime_object[i]])
        
labels= ("TIMESTAMP,T08,SEC,Ratio\n")
filename = "silly_"+month+"_2022"
fname = filename+".csv"

with open(fname, "a") as myfile:
    myfile.write(labels)
        
    for i in range(len(list_final)):
        strg = str(list_final[i][3]) + "," +  str(list_final[i][2]) + "," +  str(list_final[i][1]) + "," +  str(list_final[i][0])
        myfile.write(strg)
        myfile.write("\n") 
        
print("\nFile created in SWAN project\n")


# In[ ]:




